using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CameraSelector : MonoBehaviour
{
    public RenderTexture radar;

    [SerializeField] GameObject clothesObject, feetObject, fullCharacter;
    [SerializeField] Material clothesMaterial, harnessMaterial;
    [SerializeField] TMP_Text onOff;

    [SerializeField] Camera[] cameras;
    [SerializeField] ParticleSystem squirt, semen;
    bool clothesOn;

    [SerializeField] GameObject dildoObject1, dildoObject2, dildoHand;
    bool dildoOn, dildoOn2, dildoOn3;
    [SerializeField] Transform right, left, positioner;
    [SerializeField] Transform characterRight, characterLeft, characterPositioner, annotableScreen, annotableRight, annotableLeft;
    bool isRight;
    // Start is called before the first frame update
    void Start()
    {
        dildoObject1.SetActive(false);
        dildoObject2.SetActive(false);
        dildoHand.SetActive(false);
        squirt.Stop();
        semen.Stop();
        feetObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SelectCamera(int camNum)
    {
        switch (camNum)
        {
            case 0:
                cameras[0].targetTexture = radar;
                cameras[1].targetTexture = null;
                cameras[2].targetTexture = null;
                cameras[3].targetTexture = null;
                break;
            case 1:
                cameras[1].targetTexture = radar;
                cameras[0].targetTexture = null;
                cameras[2].targetTexture = null;
                cameras[3].targetTexture = null;
                break;
            case 2:
                cameras[2].targetTexture = radar;
                cameras[1].targetTexture = null;
                cameras[0].targetTexture = null;
                cameras[3].targetTexture = null;
                break;
            case 3:
                cameras[3].targetTexture = radar;
                cameras[1].targetTexture = null;
                cameras[2].targetTexture = null;
                cameras[0].targetTexture = null;
                break;
        }
    }

    public void Squirt(int type)
    {
        switch (type)
        {
            case 1:
                dildoObject1.GetComponentInChildren<Animator>().SetBool("freeze", true);
                squirt.Stop();
                squirt.Play();
                break;
            case 2:
                dildoObject2.GetComponentInChildren<Animator>().SetBool("freeze", true);
                semen.Stop();
                semen.Play();
                break;
        }
        StartCoroutine(StopSquirt());
    }
    IEnumerator StopSquirt()
    {
        yield return new WaitForSeconds(8);
        squirt.Stop();
        semen.Stop();
        dildoObject1.GetComponentInChildren<Animator>().SetBool("freeze", false);
        dildoObject2.GetComponentInChildren<Animator>().SetBool("freeze", false);
    }

    public void Clothes()
    {
        if (!clothesOn)
        {
            fullCharacter.GetComponent<SkinnedMeshRenderer>().material = clothesMaterial;
            clothesOn = true;
            clothesObject.SetActive(true);
            feetObject.SetActive(false);
            onOff.text = "Quitar ropa";
        }
        else
        {
            fullCharacter.GetComponent<SkinnedMeshRenderer>().material = harnessMaterial;
            clothesOn = false;
            clothesObject.SetActive(false);
            feetObject.SetActive(true);
            onOff.text = "Poner ropa";
        }
    }
    public void Dildo(int pos)
    {
        switch (pos)
        {
            case 1:
                if (!dildoOn)
                {
                    dildoOn = true;
                    dildoObject1.SetActive(true);
                }
                else
                {
                    dildoOn = false;
                    dildoObject1.SetActive(false);
                }
                break;
            case 2:
                if (!dildoOn2)
                {
                    dildoOn2 = true;
                    dildoObject2.SetActive(true);
                }
                else
                {
                    dildoOn2 = false;
                    dildoObject2.SetActive(false);
                }
                break;
            case 3:
                if (!dildoOn3)
                {
                    dildoOn3 = true;
                    dildoHand.SetActive(true);
                }
                else
                {
                    dildoOn3 = false;
                    dildoHand.SetActive(false);
                }
                break;
        }
    }

    public void CloseApp()
    {
        Application.Quit();
    }

    public void Reposition()
    {
        if (!isRight)
        {
            positioner.position = right.position;
            annotableScreen.position = annotableRight.position;
            characterPositioner.position = characterLeft.position;
            isRight = true;
        }
        else
        {
            annotableScreen.position = annotableLeft.position;
            positioner.position = left.position;
            characterPositioner.position = characterRight.position;
            isRight = false;
        }
    }
}
