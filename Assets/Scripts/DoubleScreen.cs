using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleScreen : MonoBehaviour
{
    [SerializeField] int displayID;
    // Start is called before the first frame update
    void Start()
    {
        if(Display.displays.Length > 1)
        {
            Display.displays[displayID].Activate();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
